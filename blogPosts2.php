
<?php 
require_once('Blog-The-New-Easy/es/wp-config.php'); 
$my_query = new WP_Query('showposts=2'); 
/*$response = array();
$posts = array();
while ($my_query->have_posts()) : $my_query->the_post(); 
    $do_not_duplicate = $post->ID; 
   
    $posts[] = array('title'=>the_title(), 'url'=> the_permalink(),'fecha'=> the_time('j') ."/".the_time('M')."/".the_time('Y'),'resumen'=> the_excerpt());
    echo $posts;
endwhile;
$response['posts'] = $posts;
echo json_encode($response);*/
while ($my_query->have_posts()) : $my_query->the_post(); ?>



			<article class="postindex">
                <figure>
                   
  					<?php if ( has_post_thumbnail() ) { 
    				the_post_thumbnail( 'mi-miniatura' ); 
    				}?>
                </figure>
                <h2>
					<?php the_title(); ?>
                </h2>
                <p>
                	<?php $myExcerpt = get_the_excerpt();
                    $tags = array("<p>", "</p>");
                    $myExcerpt = str_replace($tags, "", $myExcerpt);
                    echo limit_words($myExcerpt,20).'...';

                    ?>
                </p>

                <a href="<?php the_permalink(); ?>">ver más</a>
            </article>


		<?php endwhile; ?>

<?php
function echo_first_image( $postID ) {
	$args = array(
		'numberposts' => 1,
		'order' => 'ASC',
		'post_mime_type' => 'image',
		'post_parent' => $postID,
		'post_status' => null,
		'post_type' => 'attachment',
	);

	$attachments = get_children( $args );

	if ( $attachments ) {
		foreach ( $attachments as $attachment ) {
			$image_attributes = wp_get_attachment_image_src( $attachment->ID, 'thumbnail' )  ? wp_get_attachment_image_src( $attachment->ID, 'thumbnail' ) : wp_get_attachment_image_src( $attachment->ID, 'full' );

			echo '<img src="' . wp_get_attachment_thumb_url( $attachment->ID ) . '" class="current">';
		}
	}
}
?>

