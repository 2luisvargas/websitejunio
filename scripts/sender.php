<?php

header('Access-Control-Allow-Origin: *');

require '../vendor/autoload.php';
use Mailgun\Mailgun;

/* Correo de los departamentos por servicios */
$mails = [
    'operadores' => 'ventascarriers@cibersys.com', //'troyaluis56@gmail.com',
    'corporativo' => 'corporate@cibersys.com',//'luis.troya@cibersys.com',
    'particulares' => 'soporte@cibersys.com'//'luis.troya@cibersys.com'
];

/* Correo electronico de quien recibira los mensajes */
//$to = 'luis.vargas@cibersys.com';

/* Campos recibidos del formulario de contactos*/
$from = $_POST['email'];
$nombre = createTitle('Nombre', $_POST['nombre']);
$telefono = createTitle('Telefono', $_POST['telefono']);
$asunto = createTitle('Asunto', $_POST['asunto']);
$mensaje = createTitle('Mensaje', $_POST['mensaje']);
$servicio = createTitle('Servicio', $_POST['name']);
$producto = createTitle('Producto', $_POST['pp']);

switch($_POST['name']) {
    case 'Operadores':
        $to = $mails['operadores'];
        break;
    case 'Particulares':
        $to = $mails['particulares'];
        break;
    case 'Corporativo':
        $to = $mails['corporativo'];
        break;
}

/* Creacion del cuerpo del email */
$body = '';
$body .= $nombre;
$body .= $telefono;
$body .= $asunto;
$body .= $servicio;
$body .= $producto;
$body .= $mensaje;

$subject = $_POST['asunto'];

sendEmail($from, $to, $subject, $body, false);

$from = 'Cibersys The New Easy<noreply@cibersys.com>';
$to = $_POST['email'];
$subject = 'Re: ' . $_POST['asunto'];
$body = responseText();

sendEmail($from, $to, $subject, $body, true);

/**
*  Funcion para enviar el correo
*/
function sendEmail($from, $to, $subject, $body, $shouldReturn)
{
    $mg = new Mailgun('key-0e293ffd6f9ab7e0c9bcc2273d7e564a');
    $domain = 'https://api.mailgun.net/v2/cibersys.net';

    $result = $mg->sendMessage($domain,
        array(
            'from' => $from,
            'to' => $to,
            'subject' => $subject,
            'html' => $body
        )
    );

    if ($shouldReturn) echo json_encode($result);
}

function createTitle ($title, $value)
{
    return "<br /><strong>$title :</strong> $value";
}

function responseText ()
{
    return "Muchas gracias por contactarnos a través de nuestra página web www.cibersys.com
            <br /> Para nosotros es muy importante su interés en nuestras soluciones, por lo que en breve nos comunicaremos con usted para atenderlo.
            <br /><br /> Thank you very much for contacting us through our website www.cibersys.com
            <br /> For us it is very important your interest in our solutions, so we will contact you shortly to assist you.
            <br /> Cibersys The New Easy";
}
