<?php

header('Access-Control-Allow-Origin: *');

require '../vendor/autoload.php';
use Mailgun\Mailgun;

/* Correo de los departamentos por servicios */
$mails = [
    'operadores' => 'carriers@cibersys.com',//'troyaluis56@gmail.com',
    'corporativo' => 'corporate@cibersys.com',//'luis.troya@cibersys.com',
    'particulares' => 'personal@cibersys.com'//'luis.troya@cibersys.com'
];

/* Correo electronico de quien recibira los mensajes */
//$to = 'luis.vargas@cibersys.com';

/* Campos recibidos del formulario de contactos*/
$from = $_POST['email'];
$nombre = createTitle('Nombre', $_POST['nombre']);
$telefono = createTitle('Telefono', $_POST['telefono']);
$asunto = createTitle('Asunto', $_POST['asunto']);
$mensaje = createTitle('Mensaje', $_POST['mensaje']);
$servicio = createTitle('Servicio', $_POST['name']);
$producto = createTitle('Producto', $_POST['pp']);

switch($_POST['name']) {
    case 'Operadores':
        $to = $mails['operadores'];
        break;
    case 'Particulares':
        $to = $mails['particulares'];
        break;
    case 'Corporativo':
        $to = $mails['corporativo'];
        break;
}

/* Creacion del cuerpo del email */
$body = '';
$body .= $nombre;
$body .= $telefono;
$body .= $asunto;
$body .= $servicio;
$body .= $producto;
$body .= $mensaje;

$subject = $_POST['asunto'];

sendEmail($from, $to, $subject, $body);

/**
*  Funcion para enviar el correo
*/
function sendEmail($from, $to, $subject, $body)
{
    $mg = new Mailgun('key-0e293ffd6f9ab7e0c9bcc2273d7e564a');
    $domain = 'https://api.mailgun.net/v2/cibersys.net';

    $result = $mg->sendMessage($domain,
        array(
            'from' => $from,
            'to' => $to,
            'subject' => $subject,
            'html' => $body
        )
    );

    echo json_encode($result);
}

function createTitle ($title, $value)
{
    return "<br /><strong>$title :</strong> $value";
}