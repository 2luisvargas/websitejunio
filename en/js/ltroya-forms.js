$(function () {

    //$('#mensaje').val('Envio del formulario desde el sitio de cibersys/index. \n\nLos felicito, me gusta mucho el nuevo sitio.');

    $('#contactForm').submit(function (event) {
        // Formulario a enviar al servidor
        var form = $(this);

        // Label para mostrar la respuesta del servidor
        var msg = $('#form-status');

        // Determinar si el formulario esta listo para ser enviado al servidor
        if (form.valid()) {

            // Ajax. Envio del formulario
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: fixNewLines($(this).serialize()),
                success: function (data) {
                    //console.log('Data', data);
                    showMessage(msg, 'Su mensaje fue enviado correctamente', 'green');

                    // Limpiar el formulario luego de ser enviado
                    form[0].reset();

                    // Funcion para ocultar los productos
                    hideProducts();
                },
                error: function (xhr, err) {
                    showMessage(msg, 'Su mensaje no pudo ser enviado. Por favor, intente nuevamente', 'red');
                }
            });

            // Prevenir el submit del formulario y recarga de la pagina
            event.preventDefault();
        }
    });

    // Radio buttons que contienen los servicios de cibersys
    var radioServiceButtons = $('#contactForm').find('input[type="radio"][name="name"]');

    // Radio buttons que contienen los productos de cibersys
    var radioProductButtons = $('#contactForm').find('input[type="radio"][name="pp"]');

    radioServiceButtons.change(function () {
        var products = ['operadoresOpc', 'corporativoOpc'];

        //console.log(/);
        radioProductButtons
            .prop('required', false)
            .filter(':checked')
            .prop('checked', false);

        products.forEach(function (product) {
            $('#'+product).hide('slow');
        });

        var option = $(this).data('product');
        $('#'+option)
            .show('slow')
            .find(':radio')
            .prop('required', true);


    });

    function hideProducts () {
        var products = ['operadoresOpc', 'corporativoOpc'];
        products.forEach(function (product) {
            $('#'+product).hide('slow');
        });
    }

    function showMessage (element, message, color) {
        element
            .text(message)
            .css({ color: color})
            .show('slow')
            .delay(5000)
            .hide('slow');
    }
    /**
     * Funcion para enviar por correos los saldos de lineas de la caja de mensajes
     * @param string Cadena donde se reemplazara el caracter enviado en la url de %0D a <br />
     * @returns {string} Cadena con los caracteres reemplazados
     */
    function fixNewLines(string) {
        return string.replace(/%0D/g, '<br />');
    }

});
